# Design of Worst-Case-Optimal Spaced Seeds

(c) 2022-2023 Sven Rahmann & Jens Zentgraf
See LINCENSE file for licensing details.

## Introduction

This repository contains sofware that accompanies the research paper "Design of Worst-Case-Optimal Spaced Seeds".
There are two different parts:

1. Software to compute the worst-case change distribution for a given mask or class masks with a given shape. This software is in directory `spacedseeds/`, and it should be run in a conda environment defined by `spacedseeds.yml`. In particular, it uses `gurobipy` to create the integer linear program (ILP) described in the paper, and it needs an installation of Gurobi with an active license (academic licenses are available for free from Gurobi, Inc.).

2. A challenging dataset of DNA reads (each read with 6 substitutions), and a workflow that runs several tools (and in our case, masks) against these reads. The results are described in the paper and can be reproduced with the workflow provided here. It has to be run in a conda environment defined by `gapmap.yml` and uses Snakemake for workflow management.

Both tools are described in more detail below.

## Computing worst-case change distributions

### Setup

We recommend to use the conda/mamba package manager to install and manage environments in which to run our software.
Current best practice is to use the most recent [`miniforge` release](https://github.com/conda-forge/miniforge/releases), which already provides the much faster mamba frontend.

Assuming that miniforge is installed and our repository has been cloned, proceed to create the `spacedsseds` conda environment (using mamba, which can be replaced by conda, if mamba is not available), activate it and run the tools that computes worst-case change distributions:

```bash
mamba env create -f spacedseeds.yml
conda activate spacedseeds
cd spacedseeds
python worst_changes.py --help
```

This should print the command-line interface help for the tool.
If, instead, you get something like `ModuleNotFoundError: No module named 'gurobipy'`, then you have not correctly installed or activated the environment from the top-level `spacedseeds.yml` environment file.
If you get a message that you do not have a valid Gurobi license, you need to obtain the Gurobi optimizer software, together with a (new) license.
Academic and limited-time evaluation licenses are free from Gurobi, Inc.
We are not in any way affiliated to Gurobi, Inc.

### Evaluating a mask

Let's compute the properties of a specific short mask of shape (19,29):
`###_###_#__#_###_#__#_###_###` for sequences of length 100 for every number of changes up to 7. It is important to quote the mask as shown:

```bash
python worst_changes.py --mask "###_###_#__#_###_#__#_###_###" -n 100 -c 1 2 3 4 5 6 7
```

The output should be like
```
Set parameter Username
Academic license - for non-commercial use only - expires 2023-12-03
k  w  mask  n  tol  chg  minhits  _  mincov  _
19 29 ###_###_#__#_###_#__#_###_### 100 6  1  53 -1  95 -1
19 29 ###_###_#__#_###_#__#_###_### 100 6  2  34 -1  84 -1
19 29 ###_###_#__#_###_#__#_###_### 100 6  3  21 -1  65 -1
19 29 ###_###_#__#_###_#__#_###_### 100 6  4  10 -1  58 -1
19 29 ###_###_#__#_###_#__#_###_### 100 6  5  5 -1  48 -1
19 29 ###_###_#__#_###_#__#_###_### 100 6  6  1 -1  19 -1
19 29 ###_###_#__#_###_#__#_###_### 100 6  7  0 -1  0 -1
```

The first two lines are from Gurobi and cannot be suppressed at the moment; they should be ignored.
The third line is the header line for the **whitespace-separated** output below.
Each of the following lines contains information about a particular mask for a particular sequence length for a particular number of changes. The order is:

- `k`: weight of the mask (number of `#`)
- `w`: width of the mask (its total length)
- `mask`: mask in string form $\mu$ (consisting of significant positions `#` and don't-care positions `_`)
- `n`: the sequence length for which the following information holds
- `tol`: the maximally tolerated number of changes (C* in the paper); using more changes will result in all $\mu$-mers being changed
- `chg`: the number of changes for which the following information holds
- `minhits`: number of unchanged $\mu$-mers when distributing `chg` changes across `n` positions in the worst possible way (to minimize the number of hits).
- `_`: unused column (always contains `-1`), but may contain the solving time(s) when used with option `--times`.
- `mincov`: number of covered positions (out of `n`) when distributing `chg` changes across `n` positions in the worst possible way (to minimize the number of covered positions).

Note that this output format seems redundant, as `k`, `w`, `mask`, `n`, `tol` are repeated in every row.
However, the format is designed to allow concatenation to further process masks with different shapes for possibly different sequence lengths, and/or to visualize the results by loading the file as a `DataFrame` into `pandas` or `R`.

Also note that the actual change placement is not reported, as this would clutter the output. It can be obtained (together with ILP solver progress) by using `--ilpdetails` as an option.


### Investigating details

Let's investigate this mask in details for 5 changes:
```bash
python worst_changes.py --mask "###_###_#__#_###_#__#_###_###" -n 100 -c 5 --times --ilpdetails
```

The output should be similar to the following
```
Set parameter Username
Academic license - for non-commercial use only - expires 2023-12-03
k  w  mask  n  tol  chg  minhits  _  mincov  _
Set parameter Threads to value 30
Gurobi Optimizer version 10.0.3 build v10.0.3rc0 (linux64)

CPU model: AMD Ryzen 9 5950X 16-Core Processor, instruction set [SSE2|AVX|AVX2]
Thread count: 16 physical cores, 32 logical processors, using up to 30 threads

[--- many lines of ILP solving progress clipped ---]

19 29 ###_###_#__#_###_#__#_###_### 100 6  5  5 0.29  48 9.80
CHGS    ..........................X.......X..................X.........X.......X............................
KMHITS  .X...X..........X...............................................X...X... 5
CHGS    ..................................X.......X....................X....X..X............................
COVPOS  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.XXXXXXX..X.XXX.XXX............................................... 48
```

We see that the MinHits ILP is created and solved very fast (0.29 seconds), whereas the MinCov ILP for this instance is much harder (9.80 seconds).

The first line starting with `CHGS` indicates with `X` the positions where the changes have to be placed to minimize hits, and the following line `KMHITS` indicates with `X` the 5 starting positions of the hits (unchanged $\kappa$-mers).

The next line starting with `CHGS` again indicates with `X` the positions where the changes have to be placed to minimize coverage (usually, these are different from the positions used to minimize hits!), and the and the following line `COVPOS` indicates with `X` the 48 covered positions.

Note that there may be other co-optimal solutions for both problems.


### Evaluating many masks at once

Typically, we want to investigate a whole class of masks (with constant shape), so we can then select the best masks out of this class.
Let's do this for the shape (19, 29), for 5 changes and sequence length 100, keeping track of the solving times.
For this use case, it is not recommended to use the `--ilpdetails` option, as it will make the output very cluttered and not easily parsable during downstream analysis.

```bash
python worst_changes.py -k 19 -w 29 -n 100 -c 5 --times
```

This command will evaluate all 1287 symmetric masks that start and end with `#` of shape (19, 29) for 5 changes in 100 positions and report the tolerated number of changes (`tol`, which may be below, equalt to, or above 5), and the minimal objective values for MinHits (column `minhits`) and MinCov (column `mincov`), together with the ILP solving times (columns marked `_`).
The output looks as follows (times may vary considerably depending on the CPU and Gurobi version used):
```
Set parameter Username
Academic license - for non-commercial use only - expires 2023-12-03
k  w  mask  n  tol  chg  minhits  _  mincov  _
19 29 #########_____#_____######### 100 5  5  2 0.09  22 1.23
19 29 ########_#____#____#_######## 100 5  5  2 0.10  24 1.22
19 29 ########__#___#___#__######## 100 5  5  3 0.22  29 1.94
19 29 ########___#__#__#___######## 100 5  5  3 0.21  30 2.52
19 29 ########____#_#_#____######## 100 5  5  3 0.10  27 1.67
19 29 ########_____###_____######## 100 5  5  2 0.10  22 1.22
19 29 #######_##____#____##_####### 100 5  5  2 0.10  24 1.04
[...]
```

We can also evaluate a range of widths and changes at the same time:
```bash
python worst_changes.py -k 19 --wmin 19 --wmax 23 -n 100 -c 4 5 6 --times
```
This will evaluate the single (19, 19) standard 19-mer mask, the 9 symmetric (19, 21) masks, and the 45 symmetric (19, 23) masks (only odd $w$ are considered!), so 55 masks in total, each separately for 4, 5, and 6 changes, and report the solving times.
The outermost loop is over $w$, the next loop over all masks of shape $(k, w)$, and the innermost loop is over the number of changes.
The output then looks as follows (again, the first two lines have to be ignored).

```
Set parameter Username
Academic license - for non-commercial use only - expires 2023-12-03
k  w  mask  n  tol  chg  minhits  _  mincov  _
19 19 ################### 100 4  4  6 0.01  24 0.15
19 19 ################### 100 4  5  0 0.00  0 0.05
19 19 ################### 100 4  6  0 0.00  0 0.05
19 21 #########_#_######### 100 5  4  8 0.01  40 2.49
19 21 #########_#_######### 100 5  5  2 0.04  22 0.94
19 21 #########_#_######### 100 5  6  0 0.01  0 0.07
19 21 ########_###_######## 100 5  4  8 0.01  42 3.83
19 21 ########_###_######## 100 5  5  3 0.27  32 3.29
19 21 ########_###_######## 100 5  6  0 0.01  0 0.07
[...]
19 23 #########__#__######### 100 5  4  8 0.07  36 1.38
19 23 #########__#__######### 100 5  5  4 0.43  26 3.20
19 23 #########__#__######### 100 5  6  0 0.01  0 0.05
19 23 ########_#_#_#_######## 100 5  4  9 0.09  38 0.81
19 23 ########_#_#_#_######## 100 5  5  4 0.23  28 1.18
19 23 ########_#_#_#_######## 100 5  6  0 0.01  0 0.05
[...]
```

In fact, no values of changes $c$ need to be specified: If omitted, "interesting" values will be used.
However, $k$ and $n$ have to be specified and constant for a single invocation of the program.




## The challenge dataset
For the challenge dataset, we selected 5 million short reads (length n = 100) from the autosomes of t2t human reference genome.
In each read, we introduced c = 6 changes at semi-random positions, ensuring that the changes span the whole read.
Then we used bwa-mem2, minimap2, strobealign (with default parameters) and our mapper gappmed to place the reads in our modified t2t reference genome.
The modified t2t reference genome consists of the t2t genome, an Epstein-Barr virus sequence and a PhiX sequence.

### Setup

We recommend to use the conda/mamba package manager to install and manage environments in which to run our software.
Current best practice is to use the most recent [`miniforge` release](https://github.com/conda-forge/miniforge/releases), which already provides the much faster mamba frontend.

Assuming that miniforge is installed and our repository has been cloned, proceed to create the `gapmap` conda environment (using mamba, which can be replaced by conda, if mamba is not available), activate it and install the tool:

```bash
mamba env create -f gapmap.yml
conda activate gapmap
pip install -e .
```

To run the snakemake-workflow we need the [challenge dataset](https://kingsx.cs.uni-saarland.de/index.php/s/CcggyrNZWpFoZWj) and the modified [t2t reference genome](https://kingsx.cs.uni-saarland.de/index.php/s/mA9cXMYcgHsZXFb) (consisting of t2t genome, an Epstein-Barr virus sequence and a PhiX sequence).
The workflow expects the reference in the `ref` folder and the challenge dataset in the `raw` folder.
```bash
mkdir ref raw
wget -P ref https://kingsx.cs.uni-saarland.de/index.php/s/CcggyrNZWpFoZWj/download/t2t_ebv_phix.fa.gz
wget -P raw https://kingsx.cs.uni-saarland.de/index.php/s/mA9cXMYcgHsZXFb/download/challange.fq.gz
```

### Run workflow
After preparing the data, you can do a dry run of the workflow.
```bash
snakemake -j 15 --use-conda --conda-frontend mamba --scheduler greedy -n
```

This should result in the following list of jobs:
```
Job stats:
job                count
---------------  -------
all                    1
bwa_map                1
bwa_mem2_index         1
combine                1
gapmap_index          11
gapmap_map            11
minimap_index          1
minimap_map            1
plot                   1
strobealign_map        1
uncompress             1
total                 31
```

If there are no jobs listed, make sure that the challenge dataset and the reference are in the correct folders.

To execute the workflow run:
```bash
snakemake -j 15 --use-conda --conda-frontend mamba --scheduler greedy
```

Snakemake will start building environments (installing the corresponding tool (bwa-mem2, minimap2, strobealig)) for each job using mamba. (This can take some time.)
For this, we need to provide the `--use-conda` and `--conda-frontend mamba`.
The parameter `-j`  defines the number of used threads. You can adjust this according to your hardware.
After setting up the environments, Snakemake will build the required indices, map the challenge data set and create a plot.
The resulting plot can be found in `plots/sim/`.



