"""
fastcash/fastpark/fastpark_weak.py:
Efficiently mark weak k-mers by setting a "weak bit".
Weak k-mers are k-mers that have a Hamming distance 1 neighbor in the set.

Two algorithms are implemented (and a hybrid).
1. Sorting-based:
  - Extract all k-mers (or just a 'group' when memory is limited; see below)
    into a sorted list of k-mers.
    Canonical codes must be expanded into both variants of the k-mer.
  - Partition the list/group into several (#threads) sections.
    One thread processes a section.
    Therefore, sections should have equal length (1/#threads of group length).
    A section must end at a block boundary (see next).
  - Further partition the list/group into blocks.
    The k-mers in a block share a common prefix (of length approx 2k/3).
  - Compare all pairs of k-mers in a block.
    (The idea is that each block is small b/c of quadratic work).
    This will find all pairs where the difference is in the last 1/3,
    and by

"""

import numpy as np
from numba import njit, uint64, uint32, int64
from concurrent.futures import ThreadPoolExecutor, wait, FIRST_EXCEPTION, as_completed

from .dnaencode import compile_revcomp_and_canonical_code
from .lowlevel import debug


"""
Terminology:

Group: k-mers that start with a (short) common prefix
    of length group_prefix_length (0, 1 or 2).
    During marking weak k-mers, 
    a single group is kept in an in-memory array at the same time.

Section: a group is divided into several sections;
    each section is examined by one of several threads.
    If we have 9 threads, we should have 9 equally sized sections.
    Section borders must also be block borders.

Block: set of k-mers that share a (long) common prefix
    of length 'block_prefix_length' (approx. 2k/3).
    We look for HD-1 pairs of k-mers only inside blocks.
    One section consists of many blocks.

Part (of a k-mer): Approximately 1/3 of a k-mer.
    We partition a k-mer into 3 parts 
    to efficiently find its HD 1 neighbors.
    The first part (prefix) and last part (suffix) have equal length.
    The middle part may be 1 character longer or shorter, 
    depending on k mod 3.
"""


@njit(nogil=True, locals=dict(
    elem1=uint64, elem2=uint64, h=uint64, mask=uint64, onebit=uint32))
def have_hamming_dist_one(elem1, elem2):
    """
    Return True iff DNAHammingDistance(elem1, elem2) <= 1.
    elem1, elem2 must be 2-bit encoded DNA k-mers.
    """
    mask = uint64(6148914691236517205)  # Mask 0b_01...01010101
    h = elem1 ^ elem2
    h = (h | (h >> 1)) & mask
    onebit = (h & uint64(h - 1)) == uint64(0)
    return onebit


def compile_mark_weak_kmers_in_suffix(
        block_prefix_length,
        k,
        VALUEBITS=0,
        value_comp=None,
        ):
    """
    Compile and return a function 'mark_weak_kmers_in_suffix(codes)'
    that examines a sorted array of aggregated k-mer encodings and weak bit
    and decides which k-mers are weak.
    Array 'codes' stores both kmer codes and weak bit: [kmercode|weak]

    prefix_length: Configure search for HD 1 neighbors only in blocks
        where the prefix of this length is constant
    k: the k-mer size (must be >= prefix_length)
    WEAKMASK: bit mask with a single 1-bit that marks weak k-mers
    """
    if k <= block_prefix_length:
        raise ValueError(f"k={k} <= {block_prefix_length}=block_prefix_length")
    suffix_bits = 2*k - 2*block_prefix_length + 1

    if VALUEBITS and not value_comp:
        raise ValueError(f"{VALUEBITS=} > 0, but no comparison function is provided")

    @njit(nogil=True, locals=dict(
        ncodes=int64, start=int64, end=int64,
        pos=int64, pos2=int64,
        prefix=uint64,
        element=uint64,
        sec_element=uint64)
        )
    def mark_weak_kmers_in_suffix(codes, values):
        ncodes = codes.size
        start = end = 0
        while start < ncodes - 1:  # nothing to do if start == ncodes-1
            prefix = codes[start] >> suffix_bits
            while (end < ncodes) and (prefix == uint64(codes[end] >> suffix_bits)):
                end += 1
            for pos in range(start, end):

                element = codes[pos] >> 1
                found = False
                for pos2 in range(pos + 1, end):
                    # value_comp:
                    # True: check codes
                    # False: not weak depending on the values, same species, same position, ...
                    if VALUEBITS and not value_comp(values[pos], values[pos2]):
                        continue
                    sec_element = codes[pos2] >> 1
                    if have_hamming_dist_one(element, sec_element):
                        found = True
                        codes[pos2] |= 1
                if found:
                    codes[pos] |= 1
            start = end
    return mark_weak_kmers_in_suffix


def compile_swap_kmer_parts(partlengths, k):
    pl, il, sl = partlengths
    if pl != sl:
        raise ValueError(f"unsymmetric k-mer part lengths {partlengths}")
    ibl, sbl = sl, il
    suffix_mask = (4**sl - 1)
    infix_mask = (4**il -1) << (2*sl)
    prefix_mask = (4**pl - 1) << (2*(sl+il))
    SM = uint64(suffix_mask << 1)
    IM = uint64(infix_mask << 1)
    PM = uint64(prefix_mask << 1)
    suffix_back_mask = (4**il - 1)
    infix_back_mask = (4**sl -1) << (2*il)
    SBM = uint64(suffix_back_mask << 1)
    IBM = uint64(infix_back_mask << 1)

    @njit(nogil=True, locals=dict(
        pos=int64, code=uint64)
        )
    def move_middle_part_right(codes):
        for pos in range(len(codes)):
            code = codes[pos]
            codes[pos] = (code & PM) | (code & 1)\
                       | ((code & SM) << (2*il))\
                       | ((code & IM) >> (2*sl))

    @njit(nogil=True, locals=dict(
        pos=int64, code=uint64)
        )
    def build_original_kmer(codes):
        for pos in range(len(codes)):
            code = codes[pos]
            codes[pos] = (code & PM) | (code & 1)\
                       | ((code & SBM) << (2*ibl))\
                       | ((code & IBM) >> (2*sbl))

    return move_middle_part_right, build_original_kmer


def compile_grouping_functions(
        h,
        k,
        group_prefix_length,
        nextchars,
        rcmode,
        weakvalue,  # defined in the value set
        bvalues,  # Should the values be extracted?
        ):
    """
    Compile a function 'update_hashtable(ht, codes)'
    that updates the hashtable ht with the weak bit information
    from the array codes[:] where the weak bits have been set.

    Compile a function get_groupsizes(ht)
    that computes and returns an array
    groupsizes[subtable, group_prefix, nextbase]
    with the group size (number of k-mers) for a given combination
    of subtable, group prefix (in 0:4**group_prefix_length),
    and next basepair (in 0:4).

    ...
    """
    # debugprint2(f"compile_grouping_functions: rcmode={rcmode}")

    if group_prefix_length + nextchars >= k // 3:
        raise ValueError("group_prefix_length or nextchars too large: "
            f"{group_prefix_length=}, {nextchars=}, but {k//3=}")
    rc, cc = compile_revcomp_and_canonical_code(k, rcmode)
    nbuckets = h.nbuckets
    bucketsize = h.bucketsize
    get_signature_at = h.private.get_signature_at
    get_value_at = h.private.get_value_at
    get_key_sig = h.private.get_subkey_from_bucket_signature
    is_slot_empty_at = h.private.is_slot_empty_at
    get_subtable_subkey = h.private.get_subtable_subkey_from_key
    update_item = h.private.update_ssk
    get_key_from_sub_subkey = h.private.get_key_from_subtable_subkey
    shift = 2 * (k - group_prefix_length)
    nnext = 4 ** nextchars
    nextshift = shift - 2 * nextchars
    nextmask = uint64(nnext - 1)
    if nextshift <= 0:
        raise ValueError("group_prefix_length or nextchars too large: "
            f"{group_prefix_length=}, {nextchars=}, but {k//3=}, {shift=}, {nextshift=}")
    do_rev = (k % 2 == 1)  # standard case shortcut
    maybe_rev = (k % 2 == 0)  # needs add'l check

    @njit(nogil=True, locals=dict(
        code=uint64, cv=uint64, nweak=int64))
    def update_hashtable(ht, codes):
        nweak = 0
        for cv in codes:
            if cv & 1 == 0:
                continue
            code = cv >> 1
            st, subkey = get_subtable_subkey(cc(code))
            status, result = update_item(ht, st, subkey, weakvalue)
            assert status & 128 != 0
            nweak += 1
        return nweak

    @njit(nogil=True, locals=dict(
        st=uint64, p=uint64, s=uint64, sig=uint64,
        subkey=uint64, key=uint64, rev_key=uint64,
        prefix=uint64, nxt=uint64))
    def count_in_subtable(ht, groupsizes, st):
        for p in range(nbuckets):
            for s in range(bucketsize):
                if is_slot_empty_at(ht, st, p, s):
                    break
                sig = get_signature_at(ht, st, p, s)
                subkey = get_key_sig(p, sig)
                key = get_key_from_sub_subkey(st, subkey)
                rev_key = rc(key)
                # count
                prefix = (key >> shift)
                nxt = (key >> nextshift) & nextmask
                groupsizes[prefix, nxt] += 1
                if do_rev or (maybe_rev and key != rev_key):
                    prefix = (rev_key >> shift)
                    nxt = (rev_key >> nextshift) & nextmask
                    groupsizes[prefix, nxt] += 1

    @njit(nogil=True, locals=dict(
        p=uint64, s=uint64, sig=uint64,
        key=uint64, subkey=uint64, rev_key=uint64, prefix=uint64, nxt=uint64))
    def extract_group_kmers(ht, st, prefix, group_next_position, codes, values):
        for p in range(nbuckets):
            for s in range(bucketsize):
                if is_slot_empty_at(ht, st, p, s):
                    break
                sig = get_signature_at(ht, st, p, s)
                subkey = get_key_sig(p, sig)
                key = get_key_from_sub_subkey(st, subkey)
                rev_key = rc(key)
                if bvalues:
                    v = get_value_at(ht, st, p, s)
                # insert into codes table
                if (key >> shift) == prefix:
                    nxt = (key >> nextshift) & nextmask
                    codes[group_next_position[nxt]] = (key << 1)
                    if bvalues:
                        values[group_next_position[nxt]] = v
                    group_next_position[nxt] += 1

                if (do_rev or (maybe_rev and key != rev_key)) and (rev_key >> shift) == prefix:
                    nxt = (rev_key >> nextshift) & nextmask
                    codes[group_next_position[nxt]] = (rev_key << 1)
                    if bvalues:
                        values[group_next_position[nxt]] = v
                    group_next_position[nxt] += 1

    return update_hashtable, count_in_subtable, extract_group_kmers


# adapter function for sort
# Note: this is pure Python/numpy, called in ThreadPool.
# Can they actually run in parallel?
def my_sort_keys(codes):
    codes.sort(kind='quicksort')


def my_sort_keys_values(codes_values):
    assert codes_values.shape[0] == 2
    ind = np.argsort(codes_values[0, :])
    codes_values[0, :] = codes_values[0, ind]
    codes_values[1, :] = codes_values[1, ind]


@njit(nogil=True)
def get_section_borders(codes, mask, borders):
    # borders = np.empty(threads+1, dtype=np.int64)
    ncodes = codes.size
    threads = borders.size - 1
    for i in range(threads):
        borders[i] = (ncodes * i) // threads
    borders[threads] = ncodes
    for i in range(1, threads):
        while (borders[i] + 1 < ncodes) and (
                (codes[borders[i]] & mask) == (codes[borders[i] + 1] & mask)):
            borders[i] += 1
        borders[i] += 1  # first element of new block


# calculate weak k-mers ###############################
def calculate_weak_set(
        h, k,
        group_prefix_length,
        nextchars, *,
        rcmode="max",
        threads=None,
        VALUEBITS=0,
        WEAKVALUE=0,
        value_comp=None,):
    # typical: k=25, group_prefix_length=0..2, nextchars=1..2, rcmode="max"

    debugprint2, debugprint1, debugprint2 = debug.debugprint
    timestamp2, timestamp1, timestamp2 = debug.timestamp

    assert k < 32
    time_start_weak = timestamp2(msg="Begin computing weak k-mers...")
    subtables = h.subtables
    ht = h.hashtable
    if threads is None:
        threads = max(1, subtables)
    #threads = max(threads, subtables)  # threads < subtables not recommended but possible
    nnext = 4 ** nextchars

    update_hashtable, count_in_subtable, build_group \
        = compile_grouping_functions(
            h, k, group_prefix_length, nextchars, rcmode, WEAKVALUE, VALUEBITS)

    kthird = k // 3
    partlengths = [kthird] * 3  # prefix (0), infix (1), suffix (2)
    sbl = 3 * kthird
    if sbl == k - 1:
        partlengths[1] += 1
    elif sbl == k - 2:
        partlengths[0] += 1
        partlengths[2] += 1
    assert sum(partlengths) == k
    block_prefix_length = partlengths[0] + partlengths[1]
    m_block_prefix_length = partlengths[0] + partlengths[1] // 2 + partlengths[2]

    # If VALUEBITS is 0, we only compare the codes and check for a Hamming-distance of 1.
    # If VALUEBITS is >=1, we additionally need to extract the value from the hash table and
    # keep it in the same order as the keys.
    my_sort = my_sort_keys if VALUEBITS == 0 else my_sort_keys_values

    # If VALUEBITS is >= 1, we need a specific comparison function for the values.
    if VALUEBITS:
        if not value_comp:
            raise ValueError(f"{VALUEBITS=} > 0, but no comparison function is provided")

    else:
        value_comp = njit(nogil=True)(lambda x, y: True)  # True: check codes, False: not weak depending on the values

    mark_weak_kmers_in_suffix = compile_mark_weak_kmers_in_suffix(block_prefix_length, k, VALUEBITS=VALUEBITS, value_comp=value_comp)
    mark_weak_kmers_in_middle = compile_mark_weak_kmers_in_suffix(m_block_prefix_length, k, VALUEBITS=VALUEBITS, value_comp=value_comp)
    swap_right, swap_back = compile_swap_kmer_parts(partlengths, k)

    with ThreadPoolExecutor(max_workers=1 + threads) as executor:
        # Count k-mers for each combination of (subtable, prefix, nextchars)
        time_start_groupsizes = timestamp2(msg=f"Begin counting group sizes with {threads} threads")

        # Count how many kmers with prefix length group_prefix_length exist in each subtable
        gs = [np.zeros((4**group_prefix_length, nnext), dtype=np.int64) for st in range(subtables)]
        debugprint2(f"# groupsize array: {len(gs)} subtables x {gs[0].shape}")
        futures = [ executor.submit(count_in_subtable, ht, gs[st], st)
                    for st in range(subtables)]
        done, not_done = wait(futures, return_when=FIRST_EXCEPTION)
        assert len(not_done) == 0
        gs = np.array(gs)  # turn list of 2D arrays into 3D array gs[subtable, prefix, nxt]

        groupsizes = np.sum(gs, axis=(0, 2))
        gssum = np.sum(groupsizes)
        debugprint2(f"# Group sizes: {groupsizes.shape} -- sum {gssum} -- {groupsizes}")
        timestamp2(time_start_groupsizes, msg="Time for counting group sizes")
        if gssum == 0: raise ValueError("ERROR: no groups!")

        if VALUEBITS:
            bigcodes = np.zeros((2, np.amax(groupsizes)), dtype=np.uint64)
        else:
            bigcodes = np.zeros(np.amax(groupsizes), dtype=np.uint64)

        section_borders = np.empty(threads + 1, dtype=np.int64)
        for prefix in range(len(groupsizes)):
            time_start_prefixgroup = timestamp2(f"\n# Counting and bulding k-mer group with {prefix=}:")
            ncodes = groupsizes[prefix]

            # Two views, one only for codes and one for the values
            codes_values = bigcodes[:, :ncodes] if VALUEBITS else bigcodes[:ncodes]
            codes = bigcodes[0, :ncodes] if VALUEBITS else bigcodes[:ncodes]  # a view!
            values = bigcodes[1, :ncodes] if VALUEBITS else bigcodes[:ncodes]

            start_next = np.cumsum(np.sum(gs[:, prefix, :], axis=0))
            start_next[1:] = start_next[:-1]
            start_next[0] = 0
            futures = []
            start_st = np.cumsum(gs[:, prefix, :], axis=0)
            start_st[1:, :] = start_st[:-1, :]
            start_st[0, :] = 0

            starts = np.array([
                [start_next[n] + start_st[st][n] for n in range(nnext)]
                for st in range(subtables)])
            for st in range(subtables):
                assert starts[st].size == nnext
                futures.append(executor.submit(
                    build_group, ht, st, prefix, starts[st], codes, values))
            for f in as_completed(futures):
                if f.exception() is not None:
                    raise Exception(f"In future {f}: {f.exception()}")

            time_start_sort1 = timestamp2(time_start_prefixgroup, f"# Time to extract k-mer group with {prefix=}")
            sb = np.append(start_next, ncodes) # start positions of each bucket and number of codes as the end position for the last bucket
            assert sb.size == nnext + 1
            futures = []
            for i in range(nnext):
                sort_values = codes_values[:, sb[i]:sb[i + 1]] if VALUEBITS else codes[sb[i]:sb[i + 1]]
                futures.append(executor.submit(my_sort, sort_values))

            for f in as_completed(futures):
                if f.exception() is not None:
                    raise Exception(f"In future {f}: {f.exception()}")
            assert (codes_values[0] == codes).all()
            time_start_mark1 = timestamp2(time_start_sort1, f"# Time to sort k-mer group with {prefix=}")
            block_prefix_bits = partlengths[0] + partlengths[1]
            block_prefix_shift = 2*partlengths[2] + 1
            block_prefix_mask = uint64((4**block_prefix_bits - 1) << block_prefix_shift)
            get_section_borders(codes, block_prefix_mask, section_borders)
            debugprint2(f"# {section_borders=}, group size: {groupsizes[prefix]}")
            futures = [
               executor.submit(
                   mark_weak_kmers_in_suffix,
                   codes[section_borders[i]:section_borders[i + 1]],
                   values[section_borders[i]:section_borders[i + 1]],
                   ) for i in range(threads)]
            for f in as_completed(futures):
                if f.exception() is not None:
                    raise Exception(f"In future {f}: {f.exception()}")

            time_start_swap1 = timestamp2(time_start_mark1, f"# Time to mark weak k-mers from suffixes")
            futures = [
               executor.submit(swap_right, codes[section_borders[i]:section_borders[i+1]])
               for i in range(threads)]
            done, not_done = wait(futures, return_when=FIRST_EXCEPTION)
            assert len(not_done) == 0
            time_start_sort2 = timestamp2(time_start_swap1, f"# Time to bit-swap k-mers")

            # Re-sort after bit-swap
            block_prefix_bits = partlengths[0]
            block_prefix_shift = 2 * (partlengths[1] + partlengths[2]) + 1
            block_prefix_mask = uint64((4**block_prefix_bits - 1) << block_prefix_shift)
            get_section_borders(codes, block_prefix_mask, section_borders)

            futures = []
            for i in range(threads):
                sort_values = codes_values[:, section_borders[i]:section_borders[i + 1]] if VALUEBITS else codes[section_borders[i]:section_borders[i + 1]]
                futures.append(executor.submit(my_sort, sort_values))

            for f in as_completed(futures):
                if f.exception() is not None:
                    raise Exception(f"In future {f}: {f.exception()}")
            time_start_mark2 = timestamp2(time_start_sort2, f"# Time to re-sort after bit-swap")

            # Define sections, mark k-mers (from middle part)
            block_prefix_bits = partlengths[0] + partlengths[2]
            block_prefix_shift = 2 * partlengths[1] + 1
            block_prefix_mask = uint64((4**block_prefix_bits - 1) << block_prefix_shift)
            get_section_borders(codes, block_prefix_mask, section_borders)
            debugprint2(f"new {section_borders=};  group size: {groupsizes[prefix]}")
            futures = [
                executor.submit(
                    mark_weak_kmers_in_middle,
                    codes[section_borders[i]:section_borders[i + 1]],
                    values[section_borders[i]:section_borders[i + 1]],
                ) for i in range(threads)]
            for f in as_completed(futures):
                if f.exception() is not None:
                    raise Exception(f"In future {f}: {f.exception()}")
            time_start_swap2 = timestamp2(time_start_mark2, "# Time to mark weak k-mers from middle part")

            # Bit-swap k-mers back
            futures = [
               executor.submit(swap_back, codes[section_borders[i]:section_borders[i + 1]])
               for i in range(threads)]
            for f in as_completed(futures):
                if f.exception() is not None:
                    raise Exception(f"In future {f}: {f.exception()}")
            time_start_update = timestamp2(time_start_swap2, "# Time to bit-swap k-mers back")

            # Update hash table
            weak_kmers = update_hashtable(ht, codes)
            _ = timestamp2(time_start_update, "# Time to update hash table")
            _ = timestamp2(time_start_prefixgroup, f"# Total time for k-mer group with {prefix=}")
    _ = timestamp2(time_start_weak, "\n# TOTAL TIME for weak k-mers")
    # That's all, folks. It ends here.
