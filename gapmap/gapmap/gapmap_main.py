"""
gapmap_main.py
gapmap: gapmap indexing and classification
by Jens Zentgraf & Sven Rahmann, 2019--2023
"""

from importlib import import_module  # dynamically import subcommand
from os import path

from jsonargparse import ArgumentParser, ActionConfigFile, SUPPRESS

from .._version import VERSION, DESCRIPTION
from ..lowlevel.debug import set_debugfunctions


def get_config_path():
    mainpath = __file__
    projectfolder = path.dirname(mainpath)
    cfgpath = path.join(projectfolder, "config")
    return cfgpath


def mymap(p):
    p.add_argument("--fastq", "-q", metavar="FASTQ", required=True, nargs="+",
        help="single or first paired-end FASTQ file to classify")
    p.add_argument("--pairs", "-p", metavar="FASTQ", nargs="+",
        help="second paired-end FASTQ file (only together with --fastq)")
    p.add_argument("--index", required=True,
        help="existing index")
    p.add_argument("--shared", action="store_true",
        help="The index should be loaded via shared memory")
    p.add_argument("--out", "-o", required=True,
        metavar="CSV", help="name of the output file.")
    p.add_argument("--compression", default="gz", choices=("none", "gz", "bz2", "xz"),
        help="Compression of output files")
    p.add_argument("--threads", "-T", "-j", metavar="INT", type=int, default=10,
        help=f"maximum number of worker threads for classification")
    p.add_argument("--chunksize", "-C", metavar="FLOAT_SIZE_MB", type=float, default=8.0,
        help=f"chunk size in MB; one chunk is allocated per thread.")
    p.add_argument("--chunkreads", "-R", metavar="INT", type=int,
        help="maximum number of reads per chunk per thread [SIZE_MB*(2**20) // 200]")
    p.add_argument("--progress", "-P", action="store_true",
        help="Show progress")
    p.add_argument("--majority_only", action="store_true",
        help="Map only based on the majority vote.")


def index(p):
    p.add_argument("--index", required=True,
        help="name of the resulting index (.hash and .info output)")
    p.add_argument("--reference", "-r", metavar="FASTA", nargs="+",
        help="reference FASTA file(s)")
    p.add_argument("-n", "--nobjects", metavar="INT",
        type=int, required=True,
        help="number of k-mers to be stored in hash table (4_500_000_000 for mouse+human)")
    k_group = p.add_mutually_exclusive_group(required=True)
    k_group.add_argument('--mask', metavar="MASK", type=str,
        help="gapped k-mer mask (quoted string like '#__##_##__#')")
    k_group.add_argument('-k', '--kmersize', dest="mask",
        type=int, metavar="INT", help=f"k-mer size")
    p.add_argument("--bucketsize", "-b", "-p",
        metavar="INT", type=int, required=True,
        help=f"bucket size, i.e. number of elements in a bucket")
    p.add_argument("--noweak", action="store_true",
        help="skip the weak computation")
    p.add_argument("--fill",
        type=float, metavar="FLOAT",
        help=f"desired fill rate (< 1.0) of the hash table")
    p.add_argument("--mapping", type=dict, help=SUPPRESS)
        # help="Dict which maps different chromosome names in the reference to a specific number. Should be defined in the config file.")
    p.add_argument("--subtables", type=int, metavar="INT",  # no default -> None!
        help="number of subtables used; subtables+1 threads are used")
    p.add_argument("--shortcutbits", "-S", metavar="INT", type=int,
        choices=(0, 1, 2), default=0,
        help="number of shortcut bits (0,1,2), default: 0")
    p.add_argument("--threads-read", type=int,  # 2?
        help="Number of reader threads")
    p.add_argument("--threads-split", type=int,  # 4?
        help="Number of splitter threads")
    p.add_argument("--hashfunctions", "--functions", metavar="SPEC", default="default",
        help=f"hash functions: 'default', 'random', or 'func0:func1:func2:func3'")
    p.add_argument("--aligned", action="store_true",
        help="use power-of-two-bits-aligned buckets (slightly faster, but larger)")
    p.add_argument("--statistics", "--stats",
        choices=("none", "summary", "details", "full"), default="summary",
        help="level of detail for statistics (none, summary, details, full (all subtables))")
    p.add_argument("--weakthreads", "-W", metavar="INT", type=int,
        help=f"calculate weak kmers with the given number of threads")
    p.add_argument("--groupprefixlength",
        metavar="INT", type=int,
        help=f"calculate weak k-mers in groups with common prefix of this length")
    p.add_argument("--maxwalk", metavar="INT", type=int,
        help=f"maximum length of random walk through hash table before failing")
    p.add_argument("--maxfailures", metavar="INT", type=int,
        help=f"continue even after this many failures; forever: -1]")
    p.add_argument("--walkseed", type=int, metavar="INT",
        help=f"seed for random walks while inserting elements")


# main argument parser #############################

def get_argument_parser():
    """
    return an ArgumentParser object
    that describes the command line interface (CLI)
    of this application
    """

    cfgpath = get_config_path()
    p = ArgumentParser(
        prog="gapmap",
        description=DESCRIPTION,
        epilog="(c) 2019+ by Algorithmic Bioinformatics, Saarland University. MIT License."
        )
    # global options
    p.add_argument("--version", action="version", version=VERSION,
        help="show version and exit")
    p.add_argument("--debug", "-D", action="count", default=0,
        help="output debugging information (repeat for more)")

    # add subcommands to parser
    subcommands = [
        ("index",
        "build index of k-mers and position in the reference",
        index,
        "gapmap_index", "main", [f"{cfgpath}/index.yaml", 'config/index.yaml', 'index.yaml']),
        ("map",
        "map the reads to an area in the reference",
        mymap,
        "gapmap_map", "main", [f"{cfgpath}/classify.yaml", 'config/classify.yaml', 'classify.yaml']),
        ]

    scs = p.add_subcommands()
    for (name, helptext, f_parser, module, f_main, default_configs) in subcommands:
        if name.endswith('!'):
            name = name[:-1]
            chandler = 'resolve'
        else:
            chandler = 'error'
        sp = ArgumentParser(prog=name, description=helptext,
            default_config_files=default_configs,)
        if name in ["classify", "index"]:
            sp.add_argument('--cfg', "--config", action=ActionConfigFile)
        sp.add_argument("--func", default=(module, f_main), help=SUPPRESS)
        f_parser(sp)
        scs.add_subcommand(name, sp, help=helptext,
            description=helptext, conflict_handler=chandler)

    return p


def main(args=None):
    p = get_argument_parser()
    pargs = p.parse_args() if args is None else p.parse_args(args)
    set_debugfunctions(debug=pargs.debug, timestamps=pargs.debug)
    sc_pargs = pargs[pargs.subcommand]
    (module, f_main) = sc_pargs.func
    m = import_module("." + module, __package__)
    mymain = getattr(m, f_main)
    mymain(sc_pargs)
