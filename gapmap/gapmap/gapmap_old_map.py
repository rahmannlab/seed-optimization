"""
gapmap_map
Mapping reads using gapped k-mers
by Jens Zentgraf & Sven Rahmann, 2019--2023
"""

from concurrent.futures import ThreadPoolExecutor, as_completed, wait

import numpy as np
from numba import njit, uint64, int64

from ..io.hashio import load_hash
from ..lowlevel import debug
from ..mask import create_mask
from ..kmers import compile_kmer_processor
from ..io.generalio import InputFileHandler, fastq_chunks
from ..dnaencode import (
    quick_dna_to_2bits,
    twobits_to_dna_inplace)


def compile_get_positions_from_read(h, mask, rcmode):
    """
    Split the sequence into gapped k-mers and query the positions in the reference from the index.
    seq: Input sequence
    ht: hash table as a numpy array
    """
    get_value = h.get_value

    @njit(nogil=True, locals=dict(
        ht=uint64[:], code=uint64, array=uint64[:], pos=uint64,
        value=uint64))
    def func(ht, code, array):
        value = get_value(ht, code)
        if array[0] <= len(array):
            array[array[0]] = value
            array[0] += 1

    k, kmer_processor = compile_kmer_processor(mask.tuple, func, rcmode=rcmode)

    return kmer_processor


def process_fastq(fastqs, pairs, csvfile, h, values, mask, rcmode, threads, *,
                  bufsize=2**23, chunkreads=(2**23) // 200):
    get_positions = compile_get_positions_from_read(h, mask, rcmode)
    w = mask.w
    k = mask.k
    pos_bits = values.pos_bits
    pos_mask = 2**pos_bits - 1
    chrom_bits = values.chrom_bits
    chrom_mask = (2**chrom_bits - 1) << pos_bits
    chrom_pos_mask = 2**(chrom_bits + pos_bits) - 1
    weak_mask = 1 << (chrom_bits + pos_bits)
    multivalue = values.multivalue

    @njit(nogil=True, locals=dict(max_count=uint64, second_max_count=uint64, max_count_strong=uint64,
            second_max_count_strong=uint64, pos_max_count=int64, pos_max_count_strong=int64,
            weak_count=uint64, strong_count=uint64, count=uint64, chrom=uint64, pos=uint64))
    def check_cluster(pos_val, offsets, fill, rev):
        max_count = 0
        second_max_count = 0
        max_count_strong = 0
        second_max_count_strong = 0
        pos_max_count = 0
        pos_max_count_strong = 0
        chrom = 0

        for i in range(fill):
            weak_count = (offsets[i, 1] & (2**32 - 1))
            strong_count = offsets[i, 1] >> 32
            count = weak_count + strong_count
            if count > max_count:
                second_max_count = max_count
                max_count = count
                pos_max_count = i
            if strong_count > max_count_strong:
                second_max_count_strong = max_count_strong
                max_count_strong = strong_count
                pos_max_count_strong = i

        if (max_count_strong > 2 and max_count_strong - second_max_count_strong > 1):
            chrom = (offsets[pos_max_count_strong, 0] & chrom_mask) >> pos_bits
            pos = (offsets[pos_max_count_strong, 0] & pos_mask)
            offset = max_count_strong

        if (max_count > 5 and max_count - second_max_count > 3):
            chrom = (offsets[pos_max_count, 0] & chrom_mask) >> pos_bits
            pos = (offsets[pos_max_count, 0] & pos_mask)
            offset = max_count

        if chrom != 0:
            pos_val[1] = 1
            pos_val[2] = chrom
            pos_val[3] = pos - offset if rev else pos
            pos_val[4] = pos + offset if not rev else pos
            return True

        return False

    @njit(nogil=True, locals=dict(min_v=uint64, min_s_v=uint64,
            max_v=uint64, max_s_v=uint64, chrom_mask=uint64,
            weak_count=uint64, strong_count=uint64, count=uint64))
    def map_reads(all_position_values):
        for pos_val in all_position_values:
            nkmers = pos_val[0]
            positions = pos_val[9: 9 + nkmers]

            # Check the size of the interval
            min_v = uint64(-1)
            min_s_v = uint64(-1)
            max_v = 0
            max_s_v = 0
            nfound = 0
            nnotfound = 0
            nmultivalues = 0
            nstrong = 0
            offset_size = 20
            offset_fill = 0
            rev_offset_fill = 0
            offsets = np.zeros((offset_size, 2), dtype=np.uint64)
            rev_offsets = np.zeros((offset_size, 2), dtype=np.uint64)
            for read_pos, pos in enumerate(positions):
                if pos == 0:
                    nnotfound += 1
                    continue

                if pos == multivalue:
                    nmultivalues += 1
                    continue

                nfound += 1

                p = pos & chrom_pos_mask

                strong = False
                if p == pos:
                    strong = True
                    nstrong += 1

                min_v = min(min_v, p)
                max_v = max(max_v, p)
                if strong:
                    min_s_v = min(min_s_v, p)
                    max_s_v = max(max_s_v, p)

                for offset_pos in range(min(offset_size, offset_fill)):
                    if offsets[offset_pos, 0] == p - read_pos:
                        if strong:
                            offsets[offset_pos, 1] += (1 << 32)
                        else:
                            offsets[offset_pos, 1] += 1
                        break
                else:
                    if offset_fill < offset_size:
                        offsets[offset_fill, 0] = p - read_pos
                        if strong:
                            offsets[offset_fill, 1] = (1 << 32)
                        else:
                            offsets[offset_fill, 1] = 1
                        offset_fill += 1

                # Do the same check in reverse order
                for rev_offset_pos in range(min(offset_size, rev_offset_fill)):
                    if rev_offsets[rev_offset_pos, 0] == p - (nkmers - read_pos):
                        if strong:
                            rev_offsets[rev_offset_pos, 1] += (1 << 32)
                        else:
                            rev_offsets[rev_offset_pos, 1] += 1
                        break
                else:
                    if rev_offset_fill < offset_size:
                        rev_offsets[rev_offset_fill, 0] = p - (nkmers - read_pos)
                        if strong:
                            rev_offsets[rev_offset_fill, 1] = (1 << 32)
                        else:
                            rev_offsets[rev_offset_fill, 1] = 1
                        rev_offset_fill += 1

            pos_val[5] = nfound - nstrong
            pos_val[6] = nstrong
            pos_val[7] = nmultivalues
            pos_val[8] = nnotfound

            if check_cluster(pos_val, offsets, offset_fill, False):
                continue

            if check_cluster(pos_val, rev_offsets, rev_offset_fill, True):
                continue

            # No specific k-mer found
            if min_v == uint64(-1):
                if nmultivalues > 1:
                    pos_val[1] = 2
                else:
                    pos_val[1] = 0  # 1 mapped at one pos
                pos_val[2] = 0
                pos_val[3] = 0
                pos_val[4] = 0
                continue

            # We want at least 2 different k-mers
            if nfound < 2:  # min_v == max_v:
                if nmultivalues > 1:
                    pos_val[1] = 2
                else:
                    pos_val[1] = 0
                pos_val[2] = 0
                pos_val[3] = 0
                pos_val[4] = 0
                continue

            # Check if all k-mers map in an interval
            if (max_v - min_v) < (nkmers + w) * 2:
                chrom = (min_v & chrom_mask) >> pos_bits
                pos_val[1] = 1  # 1 mapped at one pos
                pos_val[2] = chrom
                pos_val[3] = (min_v & pos_mask)
                pos_val[4] = (max_v & pos_mask)
                continue

            # No strong k-mer found
            if min_s_v == uint64(-1):
                if nmultivalues > 1:
                    pos_val[1] = 2
                else:
                    pos_val[1] = 0  # 1 mapped at one pos
                pos_val[2] = 0
                pos_val[3] = 0
                pos_val[4] = 0
                continue

            # We want at least 2 different strong k-mers
            if nstrong < 2:  # min_s_v == max_s_v:
                if nmultivalues > 1:
                    pos_val[1] = 2
                else:
                    pos_val[1] = 0
                pos_val[2] = 0
                pos_val[3] = 0
                pos_val[4] = 0
                continue

            # Check if the strong k-mers map in an interval
            if (max_s_v - min_s_v) < (nkmers + w) * 2:
                chrom = (min_s_v & chrom_mask) >> pos_bits
                pos_val[1] = 1  # 1 mapped at one pos
                pos_val[2] = chrom
                pos_val[3] = (min_s_v & pos_mask)
                pos_val[4] = (max_s_v & pos_mask)
                continue

            # Deletion in the reads?
            if (min_s_v & chrom_mask) >> pos_bits == (max_s_v & chrom_mask) >> pos_bits and\
               (max_s_v - min_s_v) < 50_000:
                chrom = (min_s_v & chrom_mask) >> pos_bits
                pos_val[1] = 3  # maps to multiple positions
                pos_val[2] = chrom
                pos_val[3] = (min_s_v & pos_mask)
                pos_val[4] = (max_s_v & pos_mask)
                continue

            pos_val[1] = 4
            pos_val[2] = 0
            pos_val[3] = 0
            pos_val[4] = 0

    @njit(nogil=True)
    def process_reads(ht, buffer, linemarks, positions):
        for i, (s_start, s_end, r_start, r_end) in enumerate(linemarks):
            seq = buffer[s_start:s_end]
            quick_dna_to_2bits(seq)
            get_positions(ht, seq, 0, len(seq), positions[i])
            twobits_to_dna_inplace(buffer, s_start, s_end)

    @njit(nogil=True)
    def process_chunk(threadid, ht, buffer, linemarks, positions):
        # max_read_length = max(linemarks[:, 1] - linemarks[:, 0])
        # positions = np.zeros((len(linemarks), max_read_length + 5), dtype=np.uint64)
        positions[:, 0] = 4
        process_reads(ht, buffer, linemarks, positions)
        map_reads(positions)
        # 0 not mapped
        # 1 mapped at 1 location
        # 2 mapped at multiple locations (not implemented yet)
        # 3 same chromosome but long distance
        # 4 not mapped because no rule fits
        # res = [0, 0, 0, 0, 0, 0]
        # chroms = [0]*40
        # for i in positions:
        #     res[i[1]] += 1
        #     # print(i[2])
        #     chroms[i[2]] += 1
        # print("not mapped:", res[0], "mapped:", res[1], "multi positions:", res[2], "SV: deletion:", res[3], "no rule:", res[4])
        # print(res)
        # print(chroms)

        # print(res[1] + res[2] + res[3], "of", len(linemarks), "reads (", (res[1] + res[2] + res[3]) / len(linemarks), ") mapped at one position")

        return threadid

    @njit(nogil=True)
    def split_buffer(linemarks):
        n = linemarks.shape[0]
        perthread = (n + (threads - 1)) // threads
        borders = np.empty(threads + 1, dtype=np.uint32)
        for i in range(threads):
            borders[i] = min(i * perthread, n)
        borders[threads] = n
        return borders

    ht = h.hashtable

    if pairs:
        raise NotImplementedError("paired end is not supported, yet.")
        # positions1 = np.zeros((chunkreads * threads, 150 + 5), dtype=np.uint64)
        # positions2 = np.zeros((chunkreads * threads, 150 + 5), dtype=np.uint64)
        # processed_reads = 0
        # with open(csvfile, "wt") as outfile:
        #     print("seq_id,mapping,chrom,start,end", file=outfile)
        #     with ThreadPoolExecutor(max_workers=threads) as executor:
        #         for fastq in fastqs:
        #             with InputFileHandler(fastq) as fq1,\
        #                  InputFileHandler(fastq) as fq2:
        #                 for buffer1, linemarks1, buffer2, linemarks2 in fastq_chunks_paired((fq1, fq2), bufsize=bufsize * threads, maxreads=chunkreads * threads):
        #                     borders = split_buffer(linemarks1)
        #                     max_read_length = max(max(linemarks1[:, 1] - linemarks1[:, 0]), max(linemarks2[:, 1] - linemarks2[:, 0]))

        #                     if max_read_length > positions1.shape[1] or max_read_length > positions2.shape[1]:
        #                         positions1 = np.zeros((chunkreads*threads, max_read_length + 5), dtype=np.uint64)
        #                         positions2 = np.zeros((chunkreads*threads, max_read_length + 5), dtype=np.uint64)
        #                     futures = [executor.submit(
        #                         process_chunk, i, ht, buffer1, linemarks1[borders[i]:borders[i + 1]], positions1[borders[i]:borders[i + 1]])
        #                         for i in range(threads)]
        #                     for fut in as_completed(futures):
        #                         threadid = fut.result()
        #                         current_linemarks = linemarks[borders[threadid]:borders[threadid + 1]]
        #                         current_positions = positions[borders[threadid]:borders[threadid + 1]]
        #                         for i in range(len(current_linemarks)):
        #                             seq_id = buffer[current_linemarks[i, 2]:current_linemarks[i, 0] - 1]
        #                             mapping = current_positions[i, 1]
        #                             chrom = current_positions[i, 2]
        #                             start = current_positions[i, 3]
        #                             end = current_positions[i, 4]
        #                             print(",".join([bytes(seq_id).decode(), str(mapping), str(chrom), str(start), str(end)]), file=outfile)
        #                     processed_reads += len(linemarks)
        #                     print(f"{processed_reads:_}", end="\r")

    else:
        # empty array should be fine here. We store how many values are used at the 0th position
        # [0] +1 to store the read length at the 0th position
        # [1] +1 to store a mapping result (one positions or multiple or no mapping)
        # [2] +1 to store the chromosome
        # [3] +1 to store the start of the interval in which the read is mapped
        # [4] +1 to store the end of the interval in which the read is mapped
        # [5,6,7,8] +4 nweak, nstrong, nmultivalue, nnotfound
        positions = np.zeros((chunkreads*threads, 150 + 10), dtype=np.uint64)
        processed_reads = 0
        res = [0, 0, 0, 0, 0, 0]
        chroms = [0]*40
        with open(csvfile, "wt") as outfile:
            print("seq_id,mapping,chrom,start,end,nweak,nstrong,nmultivalues,nnotfound", file=outfile)
            with ThreadPoolExecutor(max_workers=threads) as executor:
                for fastq in fastqs:
                    with InputFileHandler(fastq) as fq:
                        for buffer, linemarks in fastq_chunks(fq, bufsize=bufsize * threads, maxreads=chunkreads * threads):
                            borders = split_buffer(linemarks)
                            max_read_length = max(linemarks[:, 1] - linemarks[:, 0])
                            if max_read_length > positions.shape[1]:
                                print("Update positions array to size", max_read_length)
                                positions = np.zeros((chunkreads*threads, max_read_length + 10), dtype=np.uint64)
                            futures = [executor.submit(
                                process_chunk, i, ht, buffer, linemarks[borders[i]:borders[i + 1]], positions[borders[i]:borders[i + 1]])
                                for i in range(threads)]
                            # for fut in as_completed(futures):
                                # threadid = fut.result()
                            done, not_done = wait(futures)
                            assert len(not_done) == 0
                            for threadid in range(threads):
                                current_linemarks = linemarks[borders[threadid]:borders[threadid + 1]]
                                current_positions = positions[borders[threadid]:borders[threadid + 1]]
                                for i in range(len(current_linemarks)):
                                    seq_id = buffer[current_linemarks[i, 2]:current_linemarks[i, 0] - 1]
                                    mapping = current_positions[i, 1]
                                    chrom = current_positions[i, 2]
                                    start = current_positions[i, 3]
                                    end = current_positions[i, 4]
                                    nweak = current_positions[i, 5]
                                    nstrong = current_positions[i, 6]
                                    nmultivalues = current_positions[i, 7]
                                    nnotfound = current_positions[i, 8]
                                    print(",".join([bytes(seq_id).decode(), str(mapping), str(chrom), str(start), str(end), str(nweak), str(nstrong), str(nmultivalues), str(nnotfound)]), file=outfile)
                            for i in range(len(linemarks)):
                                res[positions[i, 1]] += 1
                                chroms[positions[i, 2]] += 1
                            processed_reads += len(linemarks)
                            print(f"{processed_reads:_}", end="\r")

        print("not mapped:", res[0], "mapped:", res[1], "multi positions:", res[2], "SV: deletion???:", res[3], "no rule:", res[4])
        # print(res)
        print(chroms)
        print(res[1] + res[2] + res[3], "of", processed_reads, "reads (", (res[1] + res[2] + res[3]) / processed_reads, ") mapped")


def main(args):
    global debugprint0, debugprint1, debugprint2
    global timestamp0, timestamp1, timestamp2
    debugprint0, debugprint1, debugprint2 = debug.debugprint
    timestamp0, timestamp1, timestamp2 = debug.timestamp
    starttime = timestamp0(msg="\n# gapmap map")

    debugprint0("\n- (c) 2019-2023 by Sven Rahmann, Jens Zentgraf, Algorithmic Bioinformatics, Saarland University")
    debugprint0("- Licensed under the MIT License")

    # Load hash table (index)
    h, values, infotup = load_hash(args.index)
    (hashinfo, valueinfo, optinfo, appinfo) = infotup
    mask = create_mask(appinfo['mask'])
    k, tmask = mask.k, mask.tuple
    assert k == appinfo['k']
    rcmode = appinfo.get('rcmode', values.RCMODE)
    if rcmode is None:
        rcmode = values.RCMODE
    if not isinstance(rcmode, str):
        rcmode = rcmode.decode("ASCII")

    process_fastq(args.fastq, args.pairs, args.out, h, values, mask, rcmode, args.threads)