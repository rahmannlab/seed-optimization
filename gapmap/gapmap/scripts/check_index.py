from argparse import ArgumentParser

from numba import njit, uint64, int64

from fastcash.io.generalio import InputFileHandler
from fastcash.io.hashio import load_hash
from fastcash.mask import create_mask
from fastcash.io.fastaio import fasta_reads
from fastcash.dnaencode import quick_dna_to_2bits, compile_revcomp_and_canonical_code

CHR_mapping= {
  b"1": 1,
  b"chr1": 1,
  b"2": 2,
  b"chr2": 2,
  b"3": 3,
  b"chr3": 3,
  b"4": 4,
  b"chr4": 4,
  b"5": 5,
  b"chr5": 5,
  b"6": 6,
  b"chr6": 6,
  b"7": 7,
  b"chr7": 7,
  b"8": 8,
  b"chr8": 8,
  b"9": 9,
  b"chr9": 9,
  b"10": 10,
  b"chr10": 10,
  b"11": 11,
  b"chr11": 11,
  b"12": 12,
  b"chr12": 12,
  b"13": 13,
  b"chr13": 13,
  b"14": 14,
  b"chr14": 14,
  b"15": 15,
  b"chr15": 15,
  b"16": 16,
  b"chr16": 16,
  b"17": 17,
  b"chr17": 17,
  b"18": 18,
  b"chr18": 18,
  b"19": 19,
  b"chr19": 19,
  b"20": 20,
  b"chr20": 20,
  b"21": 21,
  b"chr21": 21,
  b"22": 22,
  b"chr22": 22,
  b"X": 23,
  b"chrX": 23,
  b"Y": 24,
  b"chrY": 24,
  b"M": 25,
  b"MT": 25,
  b"chrM": 25,
  b"chrMT": 25,
  b"chrEBV": 26,
  b"phiX174": 27,
  }

def compile_kmer_iterator(mask, rcmode):
    k = mask.k
    shp = mask.tuple

    revcomp, ccode = compile_revcomp_and_canonical_code(k, rcmode)

    @njit(nogil=True, locals=dict(
    code=uint64, startpoint=int64, i=int64, j=int64, c=uint64))
    def kmers(seq, start, end):
        startpoints = (end - start) - shp[k - 1]
        for i in range(start, start + startpoints):
            code = 0
            for j in shp:
                c = seq[i + j]
                if c > 3:
                    break
                code = (code << 2) + c
            else:  # no break
                yield i, ccode(code)
    return kmers

def main(args):
    h, values, infotup = load_hash(args.index)
    (hashinfo, valueinfo, optinfo, appinfo) = infotup
    mask = create_mask(appinfo['mask'])
    k, tmask = mask.k, mask.tuple
    assert k == appinfo['k']
    rcmode = appinfo.get('rcmode', values.RCMODE)
    if rcmode is None:
        rcmode = values.RCMODE
    if not isinstance(rcmode, str):
        rcmode = rcmode.decode("ASCII")

    kmers = compile_kmer_iterator(mask, rcmode)

    get_value = h.get_value
    ht = h.hashtable
    pos_bits = values.pos_bits
    pos_mask = 2**pos_bits - 1
    chrom_bits = values.chrom_bits
    chrom_mask = 2**chrom_bits - 1
    chrom_pos_mask = 2**(chrom_bits + pos_bits) - 1
    multivalue = values.multivalue

    @njit()
    def check_values(ht, seq, header_num):
        for i, kmer in kmers(seq, 0, len(seq)):
            value = get_value(ht, kmer)
            if value == multivalue:
                continue
            if value == 0:
                print(header_num)
            assert value != 0
            pos = value & pos_mask
            chrom = (value >> pos_bits) & chrom_mask
            if chrom != header_num:
                print(chrom, header_num)
            if pos != i:
                print(pos, i)
            assert chrom == header_num
            assert pos == i

    with InputFileHandler(args.fasta) as fasta:
        for header, seq in fasta_reads(fasta.file):
            quick_dna_to_2bits(seq)
            header_num = CHR_mapping[header.split()[0]]
            check_values(ht, seq, header_num)




if __name__ == '__main__':
    p = ArgumentParser("Check gapmap index")
    p.add_argument("--index", required=True)
    p.add_argument("--fasta", required=True)
    main(p.parse_args())