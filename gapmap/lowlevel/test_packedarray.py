from fastcash.lowlevel.packedarray import packedarray
from fastcash.lowlevel.debug import set_debugfunctions
from fastcash.lowlevel.numbautils import numba_return_type_str


def test_losses():
    set_debugfunctions(debug=2, timestamps=2)
    for m in range(1, 1024):
        P = packedarray(size=64, maximum=m)
        n = m + 1
        if n & (n - 1) == 0:
            # n is a power of 2
            assert P.loss == 0.0
            assert P.total_bits_lost == 0


def test_empty():
    set_debugfunctions(debug=0, timestamps=0)
    maxm, size = 32, 64
    for m in range(1, maxm):
        P = packedarray(size=size, maximum=m)
        a = P.array
        for i in range(size):
            assert P.get(a, i) == 0, f"{P.get(a, i)} != 0"
    print("   ", len(numba_return_type_str(P.set)))
    print("   ", numba_return_type_str(P.get))
    assert (numba_return_type_str(P.set) == "none"), numba_return_type_str(P.set)
    assert (numba_return_type_str(P.get) == "uint64"), numba_return_type_str(P.get)


def test_full():
    set_debugfunctions(debug=2, timestamps=0)
    maxm, size = 32, 64
    for m in range(6, maxm):
        P = packedarray(size=size, maximum=m)
        a = P.array
        for i in range(size):
            P.set(a, i, m)
        for i in range(size):
            assert P.get(a, i) == m, f"{m=}, {i=}: {P.get(a, i)} != {m}"


if __name__ == "__main__":
    test_full()
    test_losses()
    test_empty()
