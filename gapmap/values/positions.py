"""
fastcash.values.positions
stores a chromosome number (1-22, X->23, Y->24, MT-> 25)
and a position in the chromosome
"""

from collections import namedtuple
from numba import njit, uint64, int64

ValueInfo = namedtuple("ValueInfo", [
    "NAME",
    "NVALUES",
    "update",
    "chrom_bits",
    "pos_bits",
    "bits",
    "get_value_from_name",
    "merge_chrom_pos",
    "weakvalue",
    "RCMODE",
    "multivalue",
])


ENCODINGS = {key: value + 1 for value, key in enumerate([str(i) for i in range(1, 23)] + ["X", "Y", "MT"])}


def get_value_from_name(name):
    chrom = name.decode().split()[0]
    if chrom not in ENCODINGS: return -1  # no encoding
    return ENCODINGS[chrom]


def initialize(chrom_bits=5, pos_bits=28, weak=1, rcmode="max"):
    bits = chrom_bits + pos_bits + weak
    if bits > 64:
        raise ValueError(f" Too many bits needed to encode the value: {bits} > 64")
    NVALUES = 2**bits
    weakvalue = 2**(bits - 1)
    multivalue = uint64(NVALUES - 1)

    @njit(nogil=True, locals=dict(
        old=uint64, new=uint64, weakvalue=uint64))
    def update(old, new):
        """

        """
        if new == weakvalue:
            return old | weakvalue

        if old != 0:
            return multivalue  # return special value, mark k-mers that occure multiple times
        else:
            return new

    @njit(nogil=True, locals=dict(
        chrom=uint64, pos=uint64, pos_bits=uint64))
    def merge_chrom_pos(chrom, pos):
        assert pos <= 2 ** pos_bits
        return uint64((chrom << pos_bits) | pos)

    return ValueInfo(
        NAME="positions",
        NVALUES=NVALUES,
        update=update,
        chrom_bits=chrom_bits,
        pos_bits=pos_bits,
        bits=bits,
        get_value_from_name=get_value_from_name,
        merge_chrom_pos=merge_chrom_pos,
        weakvalue=weakvalue,
        RCMODE=rcmode,
        multivalue=multivalue,
    )
