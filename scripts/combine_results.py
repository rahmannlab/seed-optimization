from argparse import ArgumentParser


def update_with_sam(samfile, correct):
    last_seq = None
    for line in samfile:

        # header
        if line.startswith("@"):
            continue

        qname, flag, rname, pos, *rest = line.split()
        flag = int(flag)
        pos = int(pos)

        org = qname.split("_")
        org_chrom, org_start, org_end = org[0], int(org[1]), int(org[2])

        assert qname in correct

        if last_seq == qname:
            correct[qname][-1] = 0
            # if org_chrom == rname and abs(org_start - pos) <= 20:
            #     correct[qname][-1] = 1
            continue

        last_seq = qname

        if org_chrom == rname and abs(org_start - pos) <= 20:
            correct[qname].append(1)
        else:
            correct[qname].append(0)


def update_csv(csvfile, correct, empty_dict):
    next(csvfile)
    c = 0
    for line in csvfile:
        header, *rest = line.strip().split(" ",1)
        error, *rest =  rest[0].split("] [")
        indel, *rest = rest[0].split("],")

        mapping_status, chrom, start, end, nweak, nstrong, nmultivalues, nnotfound, maxmin, rule = list(map(int, rest[0].split(",")))
        org = header.split()[0].split("_")

        org[0] = org[0][4:]
        org_chrom, org_start, org_end = list(map(int, org))

        if empty_dict:
            assert header[1:] not in correct
            correct[header[1:]] = []
        else:
            assert header[1:] in correct

        if mapping_status != 1:
            correct[header[1:]].append(0)  # not correct
            continue

        if chrom == org_chrom and org_start <= start and org_end >= end:
            correct[header[1:]].append(1)
            c += 1
        else:
            correct[header[1:]].append(0)


def write_csv(names, correct, outfile):
    print(",".join(["read"] + names), file=outfile)
    for key, value in correct.items():
        if len(value) != len(names):
            print(value, len(value))
            print(names, len(names))
        assert len(value) == len(names)
        print(",".join([key] + list(map(str, value))), file=outfile)


def write_altair_csv(names, correct, outfile):
    print(",".join(["tool", "value"]), file=outfile)
    counter = {name: 0 for name in names}

    for key, value in correct.items():
        if len(value) != len(names):
            print(value, len(value))
            print(names, len(names))
        assert len(value) == len(names)
        for i, name in enumerate(names):
            counter[name] += value[i]
    print(counter)
    for i, tool in enumerate(names):
        print(",".join([tool, str(counter[tool])]), file=outfile)


def main(args):
    correct = dict()
    empty_dict = True
    names = []
    for file in args.gapmap:
        index = file.split("/")[-1].split("-")[-1].split(".")[0]
        names.append(index)
        print(names)
        with open(file, "rt") as csvfile:
            update_csv(csvfile, correct, empty_dict)
        empty_dict = False

    with open(args.bwa, "rt") as samfile:
        names.append("bwa")
        print(names)
        update_with_sam(samfile, correct)
    with open(args.minimap, "rt") as samfile:
        names.append("minimap")
        print(names)
        update_with_sam(samfile, correct)
    with open(args.strobe, "rt") as samfile:
        names.append("strobe")
        print(names)
        update_with_sam(samfile, correct)

    if args.out.endswith(".csv"):
        args.out = args.out.rsplit(".", 1)[0]

    with open(args.out+".csv", "wt") as csvfile:
        write_csv(names, correct, csvfile)
    with open(args.out+"-altair.csv", "wt") as csvfile:
        write_altair_csv(names, correct, csvfile)


if __name__ == '__main__':
    p = ArgumentParser("Check results")
    p.add_argument("--bwa", required=True,
        help="bwa sam file")
    p.add_argument("--minimap", required=True,
        help="minimiap sam file")
    p.add_argument("--strobe", required=True,
        help="strobe sam file")
    p.add_argument("--gapmap", required=True, nargs="+",
        help="gapmap csv file")
    p.add_argument("--out", required=True,
        help="out csv")
    main(p.parse_args())
