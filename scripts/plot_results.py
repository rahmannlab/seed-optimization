# import altair as alt
# from altair_saver import save
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from argparse import ArgumentParser

mask_names = {
    "bwa": "bwa",
    "strobe": "strobe",
    "minimap": "mm2",
    "A": "A",
    "B": "B",
    "C": "C",
    "D": "D",
    "E": "E",
    "F": "F",
    "G": "G",
    "H": "H",
    "I": "I",
    "J": "J",
    "K": "K",
}


def main(args):
    if args.out.endswith(".svg"):
        args.out = args.out[:-4]

    df = pd.read_csv(args.csv)
    df["tool"] = df["tool"].map(lambda x: mask_names[x])
    df = df.sort_values(["tool"])
    mapped = [v / 5_000_000 for v in list(df["value"])]
    tools = list(df["tool"].unique())

    fig, ax = plt.subplots()
    # plt.grid(visible=True, axis="y")
    ax.set_axisbelow(True)
    ax.yaxis.grid(color="gray")

    bottom = np.zeros(len(tools))
    # add mapped
    ax.bar(tools, mapped, 0.75, label="mapped", bottom=bottom)
    bottom += mapped

    labels = [f"{int(i*1000)/10}%" for i in mapped]
    for i in range(len(labels)):
        if mapped[i] > 0.15:
            plt.text(i, mapped[i]-0.01, labels[i], ha="center", color="lightgray", va="top", rotation="vertical")
        else:
            plt.text(i, mapped[i]+0.01, labels[i], ha="center", color="gray", va="bottom", rotation="vertical")

    for tick in ax.get_xticklabels():
        tick.set_rotation(-75)

    plt.savefig(args.out + ".png", bbox_inches="tight")
    plt.savefig(args.out + ".pdf", bbox_inches="tight")
    plt.savefig(args.out + ".svg", bbox_inches="tight")


if __name__ == '__main__':
    p = ArgumentParser()
    p.add_argument("--csv", required=True,
        help="Input csv file")
    p.add_argument("--out", required=True,
        help="Output svg file")
    main(p.parse_args())
