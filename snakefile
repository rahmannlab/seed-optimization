
REFERENCE = "t2t_ebv_phix"

MASKS = {
    "A": '###################',
    "B": '####_###_#####_###_####',
    "C": '####_####_###_####_####',
    "D": '###_####__#_###__#__###_#__####_###',
    "E": '#####__#_#_#_#__#####__#_#_#_#__#####',
    "F": '###_###__#_###_#__#__#_###_#__###_###',
    "G": '#########################',
    "H": '#######__###__#_###_#__###__#######',
    "I": '#####_#_#_##_##__###__##_##_#_#_#####',
    "J": '#####_#_#__####__###__####__#_#_#####',
    "K": '##_####_###__#_###_#_###_#__###_####_##',
}

names,  = glob_wildcards("raw/{names}.fq.gz")


rule all:
    input:
        [f"mapped/bwa_mem2_mem/sim/{name}.sam" for name in names],
        [f"mapped/minimap_map/sim/{name}.sam" for name in names],
        [f"mapped/strobealign_map/sim/{name}.sam" for name in names],
        [f"results/gapmap/sim/{name}-{mask}.csv" for mask in MASKS for name in names],
        [f"plots/sim/correct-{name}.svg" for name in names],

rule plot:
    input:
        "plots/sim/correct-{name}-plot.csv"
    output:
        "plots/sim/correct-{name}.svg"
    log:
        "logs/sim/plot-{name}.log"
    benchmark:
        "logs/sim/plot-{name}.benchmark"
    threads: 1
    conda:
        "envs/plot.yaml"
    shell:
        "(/bin/time -v python scripts/plot_results.py --csv {input} --out {output}) &> {log}"

rule combine:
    input:
        gapmap = [f"results/gapmap/sim/{{name}}-{mask}.csv" for mask in MASKS],
        bwa = "mapped/bwa_mem2_mem/sim/{name}.sam",
        minimap = "mapped/minimap_map/sim/{name}.sam",
        strobe = "mapped/strobealign_map/sim/{name}.sam",
    output:
        "plots/sim/correct-{name}.csv",
        "plots/sim/correct-{name}-plot.csv"
    log:
        "logs/plots/sim/csv-{name}.log"
    benchmark:
        "logs/plots/sim/csv-{name}.benchmark"
    threads: 1
    shell:
        "(/bin/time -v python scripts/combine_results.py --bwa {input.bwa} --strobe {input.strobe} --minimap {input.minimap} --gapmap {input.gapmap} --out {output[0]}) &> {log}"

rule gapmap_map:
    input:
        fastq = "raw/{name}.fq.gz",
        index_h = f"ref/{REFERENCE}-{{mask}}.hash",
        index_i = f"ref/{REFERENCE}-{{mask}}.info",
    output:
        "results/gapmap/sim/{name}-{mask}.csv"
    log:
        "logs/gapmap/sim/{name}-{mask}.log"
    benchmark:
        "logs/gapmap/sim/{name}-{mask}.benchmark"
    threads:
        10
    conda:
        "gapmap"
    params:
        index = lambda wc: f"ref/{REFERENCE}-{wc.mask}"
    shell:
        "(/bin/time -v gapmap map --index {params.index} --fastq {input.fastq} --out {output} --threads {threads}) &> {log}"


rule bwa_map:
    input:
        fastq = "raw/{name}.fq.gz",
        index_fa = f"ref/{REFERENCE}.fa",
        index = [f"ref/{REFERENCE}.fa.{ext}" for ext in ["0123", "amb", "ann", "bwt.2bit.64", "pac"]],
    output:
        "mapped/bwa_mem2_mem/sim/{name}.sam"
    log:
        "logs/bwa_mem2_mem/sim/{name}.log"
    benchmark:
        "logs/bwa_mem2_mem/sim/{name}.bechmark"
    threads:
        10
    conda:
        "envs/bwa.yaml"
    shell:
        "(/bin/time -v bwa-mem2 mem -t {threads} {input.index_fa} {input.fastq} -o {output} ) &> {log}"

rule minimap_map:
    input:
        fastq = "raw/{name}.fq.gz",
        index = f"ref/{REFERENCE}.mmi",
    output:
        "mapped/minimap_map/sim/{name}.sam"
    log:
        "logs/minimap_map/sim/{name}.log"
    benchmark:
        "logs/minimap_map/sim/{name}.bechmark"
    threads:
        10
    conda:
        "envs/minimap.yaml"
    shell:
        "(/bin/time -v minimap2 -t {threads} -a -x sr {input.index} {input.fastq} -o {output} ) &> {log}"


rule strobealign_map:
    input:
        fastq = "raw/{name}.fq.gz",
        index = f"ref/{REFERENCE}.fa",
    output:
        "mapped/strobealign_map/sim/{name}.sam"
    log:
        "logs/strobealign_map/sim/{name}.log"
    benchmark:
        "logs/strobealign_map/sim/{name}.bechmark"
    threads:
        10
    conda:
        "envs/strobealign.yaml"
    shell:
        "(/bin/time -v strobealign -t {threads} {input.index} {input.fastq} -o {output} ) &> {log}"

rule uncompress:
    input:
        "ref/{genome}.fa.gz"
    output:
        "ref/{genome}.fa"
    log:
        "logs/uncompress-{genome}.log"
    benchmark:
        "logs/uncompress-{genome}.benchmark"
    shell:
        "(pigz -d -k {input}) &> {log}"

rule gapmap_index:
    input:
        "ref/{genome}.fa"
    output:
        "ref/{genome}-{mask}.hash",
        "ref/{genome}-{mask}.info",
    log:
        "logs/gapmap_index/{genome}-{mask}.log"
    benchmark:
        "logs/gapmap_index/{genome}-{mask}.benchmark"
    params:
        n = 2_700_000_000,
        mask = lambda wc: MASKS[wc.mask],
        subtables = 15,
        reader = 1,
        splitter = 2,
    threads: 18
    conda:
        "gapmap"
    shell:
        "(/bin/time -v gapmap index --mask '{params.mask}' -r {input} --index ref/{wildcards.genome}-{wildcards.mask} -n {params.n}) &> {log}"

rule bwa_mem2_index:
    input:
        "ref/{genome}",
    output:
        "ref/{genome}.0123",
        "ref/{genome}.amb",
        "ref/{genome}.ann",
        "ref/{genome}.bwt.2bit.64",
        "ref/{genome}.pac",
    log:
        "logs/bwa-mem2_index/{genome}.log",
    benchmark:
        "logs/bwa-mem2_index/{genome}.benchmark",
    conda:
        "envs/bwa.yaml"
    threads: 18
    shell:
        "(/bin/time -v bwa-mem2 index {input}) &> {log}"


rule minimap_index:
    input:
        "ref/{genome}.fa",
    output:
        "ref/{genome}.mmi",
    log:
        "logs/minimap_index/{genome}.log",
    benchmark:
        "logs/minimap_index/{genome}.benchmark",
    conda:
        "envs/minimap.yaml"
    threads: 18
    shell:
        "(/bin/time -v minimap2 -d {output} {input}) &> {log}"


rule strobealign_index:
    input:
        "ref/{genome}",
    output:
        "ref/{genome}.r{r}.sti",
    log:
        "logs/strobealign_index/{genome}-r{r}.log",
    benchmark:
        "logs/strobealign_index/{genome}-r{r}.benchmark",
    conda:
        "envs/strobealign.yaml"
    threads: 18
    shell:
        "(/bin/time -v strobealign --create-index {input} -r {wildcards.r}) &> {log}"
