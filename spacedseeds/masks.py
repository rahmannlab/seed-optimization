"""
Command line tool:
Print all symmetric masks of a given shape range (wmin, k) to (wmax, k) to stdout.

Library functionality:
Generator function `symmetric_mask_tuples(k, wmin, wmax=None)`:
    yields all symmetric mask tuples of shapes (k, w) for wmin <= k <= wmax

TODO:
- Implement an option to also generate the non-symmetric masks.
"""


from argparse import ArgumentParser
from itertools import combinations
import random


def check_mask(mask, k=0, w=0):
    if not isinstance(mask, str):
        raise TypeError(f"mask must be of type str, not {type(mask)}")
    if not mask == mask[::-1]:
        raise ValueError(f"mask '{mask}'' is not symmetric")
    if not (mask[0] == '#' and mask[-1] == '#'):
        raise ValueError(f"first and last characters of mask '{mask}' must be '#'")
    if k > 0 and mask.count('#') != k:
        raise ValueError(f"mask '{mask}' does not have k={k} #s.")
    if w > 0 and len(mask) != w:
        raise ValueError(f"mask '{mask}' does not have width w={w}.")


def mask_to_tuple(mask):
    """Convert a mask string (with # and _) to its index tuple representation"""
    check_mask(mask)
    return tuple([i for i, c in enumerate(mask) if c == '#'])


def tuple_to_mask(tmask):
    """Convert a mask's index tuple representation to string (using # and _)"""
    w = max(tmask) + 1
    k = len(tmask)
    mask = "".join(['#' if i in tmask else '_' for i in range(w)])
    check_mask(mask, k, w)
    return mask


def _symmetric_mask_tuples(k, w):
    """generator that yields all symmetric mask tuples for shape (k, w)"""
    if w % 2 == 0:  # even width
        if k % 2 != 0:
            raise ValueError(f"For even {w=}, we need even k, but {k=} was given")
        m = w // 2 
        r = (k - 2) // 2  # one half: choose r out of m positions
        for c in combinations(range(1, m), r):
            t = [0] + list(c) + [w - 1 - i for i in c] + [w - 1]
            yield tuple(t)
    else:  # odd width, k can be odd or even
        middle = k % 2
        m = (w - 1) // 2  # 31 -> 15
        r = (k - 2 - middle) // 2
        for c in combinations(range(1, m), r):
            if middle:
                t = [0] + list(c) + [m] + [w - 1 - i for i in c] + [w - 1]
            else:
                t = [0] + list(c) + [w - 1 - i for i in c] + [w - 1]
            yield tuple(t)


def symmetric_mask_tuples(k, wmin, wmax=None):
    if wmax is None:
        wmax = wmin
    if wmax < wmin:
        raise ValueError(f"Error: symmetric_mask_tuples: {wmin=} > {wmax=}")
    for w in range(wmin, wmax + 1):
        if k % 2 != 0 and w % 2 == 0:
            continue  # odd k and even w not possible
        yield from _symmetric_mask_tuples(k, w)


def get_random_symmetric_mask(k, w):
    """Return a symmetric random mask of width `w` and weight `k`"""
    if w % 2 == 0:  # even width
        if k % 2 != 0:
            raise ValueError(f"For even w={w}, we need even k, but k={k} was given")
        m = w // 2 
        r = (k - 2) // 2  # one half: choose r out of m positions
        half = frozenset([0] + sorted(random.sample(range(1, m), r)))
        masklist = ['#' if (i in half or (w - 1 - i) in half) else '_' for i in range(w)]
    else:
        middle = k % 2
        m = (w - 1) // 2  # 31 -> 15
        r = (k - 2 - middle) // 2
        half = frozenset([0] + sorted(random.sample(range(1, m), r)))
        masklist = ['#' if (i in half or (w - 1 - i) in half) else '_' for i in range(w)]
        if middle:
            masklist[m] = '#'
    return ''.join(masklist)


def symmetric_mask_samples(k, wmin, wmax, nsamples, seed=17):
    """
    Yield `nsamples` random samples of symmetric masks of shape (w,k)
    for each w = wmin ... wmax, using the given random seed
    """
    if wmax is None:
        wmax = wmin
    if wmax < wmin:
        raise ValueError(f"Error: symmetric_mask_tuples: {wmin=} > {wmax=}")
    random.seed(seed)
    for w in range(wmin, wmax + 1):
        if k % 2 != 0 and w % 2 == 0:
            continue  # odd k and even w not possible
        for i in range(nsamples):
            mask = get_random_symmetric_mask(k, w)
            check_mask(mask, k, w)
            yield mask_to_tuple(mask)


def get_argument_parser():
    p = ArgumentParser(description="Generate and print all masks of a given shape")
    p.add_argument("-k", "--weight", type=int, required=True,
        help="number k of significant positions")
    p.add_argument("-w", "--wmin", "--width", "--span", required=True,
        type=int,
        help="width w of mask (or minimum width)")
    p.add_argument("--wmax", type=int,
        help="maximum width of mask")
    p.add_argument("--random", type=int,
        help="do not enumerate all masks; just run this number of random samples")
    p.add_argument("--seed", type=int, default=17,
        help="random seed for use with --random (default: 17)")
    return p


def main(args):
    k, wmin, wmax = args.weight, args.wmin, args.wmax
    if args.random is None:
        all_masks = symmetric_mask_tuples(k, wmin, wmax)
    else:
        all_masks = symmetric_mask_samples(k, wmin, wmax, args.random, args.seed)
    for tmask in all_masks:
        mask = tuple_to_mask(tmask)
        print(mask)


if __name__ == "__main__":
    p = get_argument_parser()
    args = p.parse_args()
    main(args)
