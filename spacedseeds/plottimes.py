"""
To run the plotting script, it is necessary to ADDITIONALLY install
the following packages into the spacedseeds environment: pandas, altair
This should be done as follows: In the active environment, run:

mamba install pandas altair

"""

from argparse import ArgumentParser
from os.path import splitext
import pandas as pd
import altair as alt


def show_hists(prefix, df):
    c1 = alt.Chart(df).mark_bar().encode(
        x=alt.X('minhits:O'),
        y=alt.Y('count()').title('number of masks'),
        )
    c2 = alt.Chart(df).mark_bar().encode(
        x=alt.X('mincov:O'),
        y=alt.Y('count()').title('number of masks'),
        )
    c = c1 | c2
    c.save(f'{prefix}.histograms.html')


def show_times(prefix, df):
    c1 = alt.Chart(df).mark_boxplot().encode(
        x='minhits:O',
        y=alt.Y('time_hits').title('time [s]'),
        )
    c2 = alt.Chart(df).mark_boxplot().encode(
        x='mincov:O',
        y=alt.Y('time_cov').title('time [s]'),
        )
    c = c1 | c2
    c.save(f'{prefix}.times.html')


def scan_file(fname):
    skip = 0
    with open(fname, "rt") as f:
        for line in f:
            skip += line.lower().startswith(
                ('set parameter', 'academic')
                )
    return skip


def get_argument_parser():
    p = ArgumentParser(description="tool for plotting ILP solving times")
    p.add_argument("table",
        help="file with output worst_changes.py in whitespace-delimited format")
    # First lines of file might start with junk from gurobi
    # Set parameter Username
    # Academic license - for non-commercial use only - expires 2023-08-04
    # k  w  mask  n  tol  chg  minhits  _  mincov  _
    p.add_argument("--out", "-o", metavar="PREFIX",
        help="prefix for output files ({prefix}.histograms.html, {prefix}.times.html)")
    return p


def main(args):
    toskip = scan_file(args.table)
    print(f"Skipping {toskip} rows (junk from Gurobi)")
    df = pd.read_csv(args.table, sep=r'\s+',
        header=0, skiprows=toskip,
        names='k w mask n tol chg minhits time_hits mincov time_cov'.split())
    print(f"Shape of DataFrame: {df.shape}")
    out = args.out if args.out is not None else splitext(args.table)[0]
    show_hists(out, df)
    show_times(out, df)


if __name__ == "__main__":
    p = get_argument_parser()
    args = p.parse_args()
    main(args)
