"""
Mask optimization, phase 1:

Given a number c of changes and a sequence length n,
For all masks of a given shape (k, w), solve two optimization problems:
Find an adversarial distribution of the c changes over n positions to
  1. minimize the number of intact k-mers
  2. minimize the number of positions covered by intact k-mers

Output goes to stdout and can/should be redirected to a file.
Summary output is tabular; columns are separated by whitespace.
The first line is a table header.

Output for a single mask contains human-readable log information and is not easily parsable.

Examples:

1. Check properties of the good (25, 29) mask '######_####_#####_####_######' for n=100:
python opt1.py --mask '######_####_#####_####_######' -n 100
(This will give full detail output, including Gurobi logs, for different numbers of changes.)

2. Check a different mask with a fixed number of changes:
python opt1.py --mask '#########_#########_#########' -n 100 -c 3

3. Find the best symmetric mask(s) of shape (k, w) = (27, 29) for c=3 changes in n=100 positions:
python opt1.py -k 27 -w 29 -c 3 -n 100

"""

import sys
from math import ceil
from datetime import datetime as dt
from argparse import ArgumentParser
from os import cpu_count

import gurobipy as gp
from gurobipy import GRB, quicksum

from masks import mask_to_tuple, tuple_to_mask, symmetric_mask_tuples, symmetric_mask_samples


VALID_MIN = ('changes', 'hits', 'coverage')


def set_initial_x(x, n, c):
    # Define a feasible solution for x[0:n], using c equidistant changes in n positions
    for i in range(n):
        x[i].Start = 0
    length = int(ceil((n - c) / (c + 1)))
    step = length + 1
    pos = [length + i * step for i in range(c)]
    for i in pos:
        x[i].Start = 1


def build_gurobi_model(mask, n, changes, minimize="changes", details=False, threads=1):
    """
    Given a gapped k-mer (`mask`), a sequence length `n`,
    and an allowed number of changes (`changes`) in the sequence,
    build a Gurobi ILP model to solve one of the following problems:
    1. If `minimize` == "changes",
       the given `changes` parameter is ignored,
       and the minimum number of changes required
       to change all gapped k-mers is computed.
    1. If `minimize` == "hits",
       minimize the number of intact gapped k-mers (or "hits")
       by placing the given number of changes in a conspiring manner.
    2. If `minimize` == "coverage",
       minimize the number of positions covered by intact gapped k-mers,
       by placing the given number of changes in a conspiring manner.
    Return the model (unsolved).
    """
    # Make sure that the mask is in tuple form.
    if minimize not in VALID_MIN:
        raise ValueError(f"Unknown minimzation mode '{minimize}', must be in {VALID_MIN}")
    if isinstance(mask, str):
        mask = mask_to_tuple(mask)
    w = mask[-1] + 1
    k = len(mask)
    model = gp.Model(f'Minimize {minimize} for {mask=}, {(w,k)=}, {n=}, {changes=}')
    model.params.outputflag = 1 if details else 0  # quiet
    model.params.logtoconsole = 1 if details else 0  # quiet
    model.params.threads = threads
    model.modelSense = GRB.MINIMIZE
    # Define n binary variables "x" to indicate chaged positions.
    x = model.addVars(n, vtype=GRB.BINARY, name='x')
    if minimize == 'changes':
        y = z = None
        obj = quicksum(x)
        model.addConstrs((quicksum(x[p + j] for j in mask) >= 1 for p in range(n - w + 1)), name="c_changes_x")
    else:
        # set_initial_x(x, n, changes)
        # Define n-w+1 binary variables to indicate unchanged k-mers
        y = model.addVars(n - w + 1, vtype=GRB.BINARY, name='y')
        model.addConstr(quicksum(x) == changes, "nchg")
        model.addConstrs((y[p] >= 1 - quicksum(x[p + j] for j in mask) for p in range(n - w + 1)), name="c_hits_y1")
        if minimize == 'hits':
            z = None
            obj = quicksum(y)
        else:
            assert minimize == 'coverage'
            # Define n binary variables to indicate positions covered by intact k-mers
            z = model.addVars(n, vtype=GRB.BINARY, name='z')
            obj = quicksum(z)
            model.addConstrs((y[p] <= 1 - x[p + j] for p in range(n - w + 1) for j in mask), name="c_hits_y0")
            model.addConstrs((y[p] <= z[p + j] for p in range(n - w + 1) for j in mask), name="c_hits_z1")

    model.setObjective(obj, GRB.MINIMIZE)
    model._variables = dict(x=x, y=y, z=z)
    model._mask = mask
    model._n = n
    model._changes = changes
    model._minimize = minimize
    model.update()
    return model


def get_changes(killchanges, n, w):
    """compute mininum and maximum intersting number of changes"""
    maxc = killchanges - 1
    nwindows = n - w + 1
    minc = min(int(ceil(nwindows / w)), maxc)
    return list(range(minc, maxc + 1))


def get_solution(model, vars, inverse=False):
    if vars is None:
        return [], ''
    sol = model.getAttr('x', vars)
    m = len(sol)
    L = [0] * m
    for k, v in sol.items():
        L[k] = int(round(v))
    if not inverse:
        s = "".join(map(str, L)).replace("0", ".").replace("1", "X")
    else:
        s = "".join(map(str, L)).replace("0", "X").replace("1", ".")
    return L, s


def get_result_from_model(model):
    opt = int(round(model.objval))
    variables = model._variables
    changes, chgs = get_solution(model, variables['x'])
    windows, wins = get_solution(model, variables['y'])
    covered, covs = get_solution(model, variables['z'])
    return opt, (changes, windows, covered), (chgs, wins, covs)


def process_mask(tmask, tolerated, n, C, details=False, time=False, threads=1):
    now = dt.now
    time_hits = time_cov = -1.0
    w, k = tmask[-1] + 1, len(tmask)
    mask = tuple_to_mask(tmask)
    D = None
    for c in C:
        if (c <= tolerated) or time:
            # minimize kmer hits
            start = now()
            model = build_gurobi_model(tmask, n, c, minimize="hits", details=details, threads=threads)
            model.optimize()
            time_hits = (now() - start).total_seconds()
            opt_hits = int(round(model.objval))
            if details:
                _, _, (chgs, wins, covs) = get_result_from_model(model)
                D = [f"CHGS    {chgs}", f"KMHITS  {wins} {opt_hits}"]

            # minimize coverage
            start = now()
            model = build_gurobi_model(tmask, n, c, minimize="coverage", details=details, threads=threads)
            model.optimize()
            time_cov = (now() - start).total_seconds()
            opt_cov = int(round(model.objval))
            if details:
                _, _, (chgs, wins, covs) = get_result_from_model(model)
                D.extend([f"CHGS    {chgs}", f"COVPOS  {covs} {opt_cov}"])
        else:
            opt_hits = opt_cov = 0
            D = []
        if D:
            print()
        if time:
            print(f"{k} {w} {mask} {n} {tolerated}  {c}  {opt_hits} {time_hits:.2f}  {opt_cov} {time_cov:.2f}")
        else:
            print(f"{k} {w} {mask} {n} {tolerated}  {c}  {opt_hits} -1  {opt_cov} -1")
        if D:
            print("\n".join(D))


def main(args):
    _ = gp.Model("dummy")
    mask = args.mask
    details = args.ilpdetails
    if mask is not None:
        all_masks = [mask_to_tuple(mask)]
        k, w = mask.count("#"), len(mask)
        wmin = wmax = w
        n = args.length
    else:
        # no mask given: have -k (--weight) and -w (--wmin), but --wmax can be None
        k, wmin, wmax, n = args.weight, args.wmin, args.wmax, args.length
        if args.random is None:
            all_masks = symmetric_mask_tuples(k, wmin, wmax)
        else:
            all_masks = symmetric_mask_samples(k, wmin, wmax, args.random, args.seed)

    print("k  w  mask  n  tol  chg  minhits  _  mincov  _")
    _changes = sorted(args.changes) if args.changes is not None else None
    times = args.times
    threads = args.threads
    if threads is None:
        threads = max(1, cpu_count() - 2)
    for tmask in all_masks:
        model = build_gurobi_model(tmask, n, None, minimize="changes", threads=threads)
        model.optimize()
        opt = int(round(model.ObjVal))
        tolerated = opt - 1
        w = tmask[-1] + 1
        changes = get_changes(opt, n, w) if _changes is None else _changes
        process_mask(tmask, tolerated, n, changes, details, times, threads)
        sys.stdout.flush()


def get_argument_parser():
    p = ArgumentParser(description="Find optimal masks")
    p.add_argument("-k", "--weight",
        type=int, metavar="K",
        help="number k of significant positions")
    p.add_argument("-w", "--wmin", "--width", "--span",
        type=int, metavar="W",
        help="width w of mask (or minimum width)")
    p.add_argument("--wmax", type=int, metavar="WMAX",
        help="maximum width of mask")
    p.add_argument("-n", "--length",
        type=int, required=True, metavar="N",
        help="sequence length n")
    p.add_argument("--mask",
        help="evaluate only the quoted mask (e.g., --mask '#_###_#'); ignores -k, -w, --random)")
    p.add_argument("--changes", "-c", type=int, nargs="+", metavar="C",
        help="compute results for the given numbers of changes (default: all resonable)")
    p.add_argument("--times", "-t", action="store_true",
        help="measure and output model setup and solving times in seconds")
    p.add_argument("--threads", "-j", type=int,
        help="number of threads for the ILP solver (CPUs - 2)")
    p.add_argument("--random", type=int, metavar="NUMBER",
        help="do not enumerate all masks; just run this number of random samples")
    p.add_argument("--seed", type=int, default=17, metavar="INTEGER_SEED",
        help="random seed for use with --random (default: 17)")
    p.add_argument("--ilpdetails", "--details", "-d", action="store_true",
        help="show detailed ILP solving logs (disrupts output!)")
    return p


if __name__ == "__main__":
    p = get_argument_parser()
    args = p.parse_args()
    if args.mask is None:
        if args.weight is None or args.wmin is None:
            p.error("Provide at least both -k and -w, or --mask")
    main(args)
